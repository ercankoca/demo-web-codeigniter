
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{

		$this->load->model('pages_model');
		$this->load->helper('tools_helper');

        $viewData = new stdClass();

        $viewData->rows = $this->pages_model->get_all(array("isActive" => "1"),"rank ASC");
        $viewData->content = $this->pages_model->get_all(array("page" => "HOMEPAGE"),"rank ASC");
        $viewData->pages_url = $this->pages_model->get_all(array("isActive" => "1"),"rank ASC");
        $viewData->contents = $this->pages_model->get_all(array("page_url" => "home"),"rank ASC");

         $viewData->title       = 'HomePage';

        // $viewData->home = $this->db->get('pages')->result();
        

		$this->load->view('home', $viewData);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
