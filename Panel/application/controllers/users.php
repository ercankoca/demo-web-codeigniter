<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model("users_model");
	}

	public function index()
	{

		$viewData = new stdClass();
		$viewData->rows = $this->users_model->get_all(array(),"id ASC");

		$this->load->view('users', $viewData);
	}

	

	public function newPage()
	{
		$this->load->view("new_user");
	}

	public function editPage($id) 
	{

		$viewData = new stdClass();

		$viewData->row = $this->users_model->get( array("id" =>  $id ));

		$this->load->view("edit_users", $viewData);
	}


	public function add()

		{
			$data = array('name' => $this->input->post('name'),
						  'surname' => $this->input->post('surname'),
						  'email'	=> $this->input->post('email'),
						   'password' => $this->input->post('password'),
						    'username' => $this->input->post('username'));
			$insert = $this->users_model->add($data);

			if ($insert) {
				redirect(base_url('users'));
			}

			else {
				redirect(base_url('users/newPage'));
			}
		}

	public function edit($id)
	{
		$data =  array('name' => $this->input->post("name") ,
						'surname' => $this->input->post("surname"),	
						'email' => $this->input->post("email"),
						'password' => $this->input->post("password"),
						'username' => $this->input->post("username")
								

		 );

		$update = $this->users_model->update(

			 array('id' => $id ), $data);

		if ($update) {
			redirect(base_url('users'));
		}

		else{
			redirect(base_url('users/editPage/$id'));
		}
	}

	public  function isActiveSetter()
	{
		$id   =  $this->input->post("id");
		$isActive = ($this->input->post("isActive")=="true") ? 1 : 0;

		$update = $this->users_model->update(

			array('id' => $id),
			array('isActive'  => $isActive)

		);
	}

	public function delete($id)
	{
		$delete = $this->users_model->delete(array("id" => $id));

		redirect(base_url("users"));
	}

	


	}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */